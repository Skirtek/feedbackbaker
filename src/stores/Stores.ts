import { createContext, useContext } from "react";
import DefaultQuestionStore, { QuestionStore } from "../questions/store/QuestionStore";


export interface IStore {
  questionStore: QuestionStore;
}

export const store: IStore = {
  questionStore: DefaultQuestionStore,
};

export const StoreContext = createContext(store);

export const useStore = () => {
  return useContext(StoreContext);
};