export interface Question {
  id?: number;
  content: string;
  order: number;
}