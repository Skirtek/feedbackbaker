import { action, makeObservable, observable, runInAction } from "mobx";
import { DefaultRequestStatus, RequestStatus } from "../../shared/api/RequestStatus";
import { Question } from "../Question";
import { QuestionFromResponse } from "../service/api/QuestionFromResponse";
import { VoteResponse } from "../service/api/VoteResponse";
import questionService, { QuestionService } from "../service/QuestionService";

export interface QuestionStore {
  questionList: Question[];
  loadQuestionListsRequestStatus: RequestStatus;
  updateQuestionListRequestStatus: RequestStatus;
  loadVotesRequestStatus: RequestStatus;
  addVoteRequestStatus: RequestStatus;

  loadQuestionList: (meetingId: string) => void;
  updateQuestionList: (meetingId: string, questions: Question[]) => void;
  loadVotes: (meetingId: string) => Promise<VoteResponse[] | undefined>;
  addVote: (questionId: number, vote: string) => void;
}

export class DefaultQuestionStore implements QuestionStore {
  constructor(private service: QuestionService, private loadQuestionListsReqStatus: RequestStatus,
    private updateQuestionListReqStatus: RequestStatus, private loadVotesReqStatus: RequestStatus,
    private addVoteReqStatus: RequestStatus) {
    makeObservable(this, {
      loadQuestionList: action,
      questionList: observable,
    });
  }

  public questionList: Question[] = [];

  public get loadQuestionListsRequestStatus() {
    return this.loadQuestionListsReqStatus;
  }

  public get updateQuestionListRequestStatus() {
    return this.updateQuestionListReqStatus;
  }

  public get loadVotesRequestStatus() {
    return this.loadVotesReqStatus;
  }

  public get addVoteRequestStatus() {
    return this.addVoteReqStatus;
  }

  public async loadQuestionList(meetingId: string) {
    if (!meetingId) {
      return;
    }

    var fetchedList = await this.loadQuestionListsReqStatus.handleAnyRequest(() =>
      this.service.getQuestionList(meetingId)
    );

    runInAction(() => {
      if (fetchedList) {
        console.log(fetchedList.data.map((x) => new QuestionFromResponse(x).question()));
        this.questionList = fetchedList.data.map((x) => new QuestionFromResponse(x).question());
      }
    });
  }

  public async updateQuestionList(meetingId: string, questions: Question[]) {
    await this.updateQuestionListReqStatus.handleAnyRequest(() =>
      this.service.updateQuestionList(meetingId, questions)
    );
  }

  public async loadVotes(meetingId: string) {
    if (!meetingId) {
      return;
    }

    var fetchedList = await this.loadVotesReqStatus.handleAnyRequest(() =>
      this.service.getVotes(meetingId)
    );

    return fetchedList?.data;
  }

  public async addVote(questionId: number, vote: string) {
    var fetchedList = await this.addVoteReqStatus.handleAnyRequest(() =>
      this.service.addVote(questionId, vote)
    );

    return fetchedList;
  }
}

export default new DefaultQuestionStore(
  questionService,
  new DefaultRequestStatus(),
  new DefaultRequestStatus(),
  new DefaultRequestStatus(),
  new DefaultRequestStatus());