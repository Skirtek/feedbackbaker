import { AxiosPromise } from "axios";
import { authorizedApi, BaseApi } from "../../shared/api/BaseApi";
import { Vote } from "../../shared/models/ReportItem";
import { Question } from "../Question";
import { QuestionListDto } from "./api/QuestionListDto";
import { VoteResponseList } from "./api/VoteResponseList";

export interface QuestionService {
  getQuestionList(meetingId: string): AxiosPromise<QuestionListDto>;
  updateQuestionList(meetingId: string, questions: Question[]): AxiosPromise;
  getVotes(meetingId: string): AxiosPromise<VoteResponseList>;
  addVote(questionId: number, vote: string): AxiosPromise<Vote[]>
}

export class DefaultQuestionService implements QuestionService {
  private getQuestionListUrl: string = "/feedback/GetQuestions";
  private updateQuestionListUrl: string = "/feedback/AddQuestions";
  private getVotesUrl: string = "/feedback/GetVotes";
  private addVoteUrl: string = "/feedback/AddVote";

  constructor(private authorizedApi: BaseApi) { }

  getQuestionList(meetingId: string): AxiosPromise<QuestionListDto> {
    return this.authorizedApi.get<QuestionListDto>(`${this.getQuestionListUrl}/${meetingId}`, {});
  }

  updateQuestionList(meetingId: string, questions: Question[]): AxiosPromise {
    return this.authorizedApi.post(`${this.updateQuestionListUrl}`, { meetingId: meetingId, questions: questions});
  }

  getVotes(meetingId: string): AxiosPromise<VoteResponseList> {
    return this.authorizedApi.get<VoteResponseList>(`${this.getVotesUrl}/${meetingId}`, {});
  }

  addVote(questionId: number, vote: string): AxiosPromise {
    return this.authorizedApi.post<Vote[]>(`${this.addVoteUrl}`, { questionId, content: vote });
  }
}

export default new DefaultQuestionService(authorizedApi);