export interface QuestionDto {
    id: number;
    meetingId: string;
    order: number;
    content: string;
}