import { QuestionDto } from "./QuestionDto";

export interface QuestionListDto {
    data: QuestionDto[];
}