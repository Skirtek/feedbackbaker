import { Question } from "../../Question";
import { QuestionDto } from "./QuestionDto";

export class QuestionFromResponse {
    constructor(private questionFromResponse: QuestionDto) {}

    question(): Question {
        return {
            id: this.questionFromResponse.id,
            content: this.questionFromResponse.content,
            order: this.questionFromResponse.order
        };
    }
}