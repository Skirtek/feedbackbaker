import { VoteResponse } from "./VoteResponse";

export interface VoteResponseList {
    data: VoteResponse[];
}