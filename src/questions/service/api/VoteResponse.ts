export interface VoteResponse {
    id: number;
    questionId: number;
    content: string;
}