import { authentication } from "@microsoft/teams-js";

class AuthProvider {
  public getAuthToken() {
    return new Promise<string>((resolve, reject) => {
      authentication.getAuthToken({
        successCallback: (token: string) => {
          resolve(token);
        },
        failureCallback: (reason: string) => {
          reject(reason);
        },
      });
    });
  }
}

export default new AuthProvider();