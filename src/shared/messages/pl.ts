const pl = {
    createQuestions: {
        header: "Utwórz nowe spotkanie",
        meetingTitle: "Tytuł spotkania",
        meetingDate: "Data spotkania",
        addQuestionsHeader: "Dodaj pytania do spotkania",
        addQuestion: "Nowe pytanie",
        defaultQuestion: "Jak się dziś czujesz?",
        confirm: "Zapisz pytania",
    },
    votingButtons: {
        allQuestionsAnswered: "Odpowiedziano na wszystkie pytania!",
    }
}

export default pl;
