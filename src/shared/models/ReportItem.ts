export interface ReportItem {
    content: string;
    votes: Vote[]; 
}

export interface Vote {
    content: string;
}