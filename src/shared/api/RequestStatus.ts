import { AxiosPromise } from "axios";
import { makeObservable, observable, runInAction } from "mobx";

export interface RequestStatus {
  isPending: boolean;
  isDone: boolean;

  handleAnyRequest<TResponse>(fetchMethod: () => AxiosPromise<TResponse>): Promise<TResponse | null>;
}

export class DefaultRequestStatus implements RequestStatus {
  _isPending: boolean = false;
  _isDone: boolean = false;

  constructor() {
    makeObservable(this, {
      _isPending: observable,
      _isDone: observable,
    });
  }

  public get isPending() {
    return this._isPending;
  }

  public get isDone() {
    return this._isDone;
  }

  public async handleAnyRequest<TResponse>(fetchMethod: () => AxiosPromise<TResponse>): Promise<TResponse | null> {
    return this.handle<TResponse>(async () => {
      const response = await fetchMethod();

      return response.data;
    });
  }

  private async handle<T>(method: () => Promise<T>) {
    try {
      runInAction(() => {
        this._isPending = true;
        this._isDone = false;
      });

      return await method();
    } catch (err) {
      // TODO Temporary, add error handling later
      alert(err);
      return null;
    } finally {
      runInAction(() => {
        this._isPending = false;
        this._isDone = true;
      });
    }
  }
}
