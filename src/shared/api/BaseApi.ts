import axios, { AxiosPromise, AxiosRequestConfig } from "axios";
import AuthProvider from "../authentication/AuthProvider";
import { rewriteAxiosRequestConfig } from "./rewriteAxiosRequestConfig";

export const authorizedApi = axios.create();

authorizedApi.interceptors.request.use(async (config: AxiosRequestConfig) => {
  // const token = await AuthProvider.getAuthToken();

  return rewriteAxiosRequestConfig({
    config: config,
    baseUrl: process.env.REACT_APP_API_BASE_URL || "",
    // accessToken: token,
  });
});

export const anonymousApi = axios.create();
anonymousApi.interceptors.request.use((config: AxiosRequestConfig) => {
  return rewriteAxiosRequestConfig({
    config: config,
    baseUrl: process.env.REACT_APP_API_BASE_URL || "",
  });
});

export interface BaseApi {
  post<T = any>(url: string, data: any, config?: AxiosRequestConfig): AxiosPromise<T>;
  get<T = any>(url: string, data: any, config?: AxiosRequestConfig): AxiosPromise<T>;
}
