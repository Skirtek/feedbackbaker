import { AxiosRequestConfig } from "axios";

export function rewriteAxiosRequestConfig(configParams: {
  config: AxiosRequestConfig;
  baseUrl: string;
  accessToken?: string;
}) {
  const params = {
    ...configParams.config,
    headers: {
      ...configParams.config.headers,
      Authorization: `Bearer ${configParams?.accessToken}`,
    },
    baseURL: configParams.baseUrl,
  };

  return params;
}
