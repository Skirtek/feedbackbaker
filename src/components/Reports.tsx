import React, { useEffect, useState } from "react";
import {
  Flex,
  Segment,
  Header,
  Text,
  CloseIcon,
  Button,
  Loader
} from "@fluentui/react-northstar";
import VerySadIcon from "../svg/VerySadIcon.svg";
import SadIcon from "../svg/SadIcon.svg";
import MixedFeelingsIcon from "../svg/MixedFeelingsIcon.svg";
import HappyIcon from "../svg/HappyIcon.svg";
import SuperHappyIcon from "../svg/SuperHappyIcon.svg";
import { ReportItem } from "../shared/models/ReportItem";
import { useStore } from "../stores/Stores";
import { useMsTeamsInMeetingApps } from "../inMeeting/hooks/hooks";

interface ReportsProps {
  onClose: () => void;
}

const Reports: React.FC<ReportsProps> = (props: ReportsProps) => {
  const [reportItems, setReportItems] = useState<ReportItem[]>([]);
  const inMeetingAppContext = useMsTeamsInMeetingApps();
  const { questionStore } = useStore();

  const mapValueToIcon = (value: number) => {
    switch (value) {
      case 1:
        return <img src={VerySadIcon} alt="very-sad-impression" />;
      case 2:
        return <img src={SadIcon} alt="sad-impression" />;
      case 3:
        return <img src={MixedFeelingsIcon} alt="mixed-feelings-impression" />;
      case 4:
        return <img src={HappyIcon} alt="happy-impression" />;
      default:
        return <img src={SuperHappyIcon} alt="super-happy-impression" />;
    }
  };

  useEffect(() => {
    (async function() {
      await questionStore.loadQuestionList(inMeetingAppContext.meetingId);
      const votes = await questionStore.loadVotes(inMeetingAppContext.meetingId);
      if (votes && votes.length) {
        setReportItems(votes.map(x => {
          return {
            content: questionStore.questionList.find(y => y.id === x.questionId)?.content!,
            votes: votes.filter(z => z.questionId === x.questionId).map(a => {
              return {
                content: a.content
              }
            })
          }
        }));
      }
    })();
  }, [inMeetingAppContext.meetingId]);

  const renderContent = () => {
    return (
      <div className="create-question-wrapper">
        <Segment>
          <Flex hAlign="end">
            <Button
              iconOnly={true}
              icon={<CloseIcon size="large" />}
              onClick={props.onClose}
            />
          </Flex>
          <Flex gap="gap.medium" column hAlign="center">
            {reportItems.length ? (
              reportItems.map((x) => (
                <Flex gap="gap.small" column hAlign="center">
                  <Header as="h3">{x.content}</Header>
                  <Flex column gap="gap.medium">
                    {x.votes.map((y) => (
                      <Flex vAlign="center" gap="gap.medium" className="vote">
                        <Text size="medium">Ocena: </Text>
                        {mapValueToIcon(Number(y.content))}
                      </Flex>
                    ))}
                  </Flex>
                </Flex>
              ))
            ) : (
              <Header as="h2">Do podanego spotkania nie dodano pytań, lub nikt jeszcze nie udzielił na nie odpowiedzi.</Header>
            )}
          </Flex>
        </Segment>
      </div>
    );
  }

  return (
    <>
      {questionStore.loadVotesRequestStatus.isPending
        ? <Loader style={{marginTop: "64px"}} />
        : renderContent()}
    </>
  )
};

export default Reports;
