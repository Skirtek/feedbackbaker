import React, { useState, useEffect } from "react";
import VerySadIcon from "../svg/VerySadIcon.svg";
import SadIcon from "../svg/SadIcon.svg";
import MixedFeelingsIcon from "../svg/MixedFeelingsIcon.svg";
import HappyIcon from "../svg/HappyIcon.svg";
import SuperHappyIcon from "../svg/SuperHappyIcon.svg";
import { useMsTeamsInMeetingApps } from "../inMeeting/hooks/hooks";
import AllQuestionsAnsweredIcon from "./icons/AllQuestionsAnsweredIcon";
import classNames from "classnames";
import { useStore } from "../stores/Stores";
import pl from "../shared/messages/pl";

const VotingButtons: React.FC = () => {
    const [activeQuestionIndex, setActiveQuestionIndex] = useState(0);
    const [isTransitioning, setIsTransitioning] = useState(false);
    const inMeetingAppContext = useMsTeamsInMeetingApps();
    const { questionStore } = useStore();

    useEffect(() => {
        (async function() {
            await questionStore.loadQuestionList(inMeetingAppContext.meetingId);
        })();
    }, [inMeetingAppContext.meetingId, questionStore]);

    const makeVote = (vote: string) => {
        questionStore.addVote(questionStore.questionList[activeQuestionIndex].id!, vote);

        setIsTransitioning(true);
        setTimeout(() => {
            setActiveQuestionIndex(activeQuestionIndex + 1);
            setIsTransitioning(false);
        }, 500);
    }

    return (
        <div className={classNames("voting-buttons-wrapper", {"fading-in": !isTransitioning, "fading-away": isTransitioning})}>
        {activeQuestionIndex <= questionStore.questionList.length - 1 ?
            <div>
                <h3>{activeQuestionIndex + 1}. {questionStore.questionList[activeQuestionIndex].content}</h3>
                <div className="icons-wrapper">
                    <img onClick={() => makeVote("1")} src={VerySadIcon} alt="very-sad-impression" />
                    <img onClick={() => makeVote("2")} src={SadIcon} alt="sad-impression" />
                    <img onClick={() => makeVote("3")} src={MixedFeelingsIcon} alt="mixed-feelings-impression" />
                    <img onClick={() => makeVote("4")} src={HappyIcon} alt="happy-impression" />
                    <img onClick={() => makeVote("5")} src={SuperHappyIcon} alt="super-happy-impression" />
                </div>
            </div>
            :
            <div>
                <h3>{pl.votingButtons.allQuestionsAnswered}</h3>
                <AllQuestionsAnsweredIcon />
            </div>}
        </div>
    );
}

export default VotingButtons;
