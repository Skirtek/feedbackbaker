import React, { useState, useRef, useEffect } from "react";
import {
    Button,
    Input,
    Segment,
    AddIcon,
    SaveIcon,
    TrashCanIcon,
    Flex,
    Loader
} from "@fluentui/react-northstar";
import pl from "../shared/messages/pl";
import { useStore } from "../stores/Stores";
import { useMsTeamsInMeetingApps } from "../inMeeting/hooks/hooks";
import { observer } from "mobx-react-lite";

interface CreateQuestionsProps {
    onChange: () => void;
}

const CreateQuestions: React.FC<CreateQuestionsProps> = observer((props: CreateQuestionsProps) => {
    const [newQuestion, setNewQuestion] = useState("");
    const newQuestionInputRef = useRef<HTMLInputElement | null>(null);
    const inMeetingAppContext = useMsTeamsInMeetingApps();
    const { questionStore } = useStore();

    useEffect(() => {
        (async function() {
            await questionStore.loadQuestionList(inMeetingAppContext.meetingId);
        })();
    }, [inMeetingAppContext.meetingId, questionStore]);

    const addNewQuestion = () => {
        if (newQuestion && !questionStore.questionList.some(x => x.content === newQuestion)) {
            questionStore.questionList.push({ content: newQuestion, order: questionStore.questionList.length });
        }

        newQuestionInputRef?.current?.focus();
        setNewQuestion("");
    };

    const saveQuestions = async () => {
        await questionStore.updateQuestionList(inMeetingAppContext.meetingId, questionStore.questionList);
    }

    const renderContent = () => (
        <div className="create-question-wrapper">
            <Segment>
                <Flex hAlign="end">
                    <Button primary onClick={props.onChange}>Raport ze spotkania</Button>
                </Flex>
                <h1>{pl.createQuestions.addQuestionsHeader}</h1>
                <ul className="questions-list">
                    {questionStore.questionList.map((x, i) =>
                        <li
                            className="questions-list-item"
                            key={x.order}
                            onClick={() => questionStore.questionList = questionStore.questionList.filter(y => y !== x)}>
                            <div>
                                {i + 1}. {x.content}
                            </div>
                            <TrashCanIcon size="large" outline />
                        </li>)}
                </ul>
                <div className="new-question">
                    <Input
                        onKeyDown={(e) => e.code === "Enter" && addNewQuestion()}
                        fluid
                        ref={newQuestionInputRef}
                        value={newQuestion}
                        onChange={(e, data) => setNewQuestion(data?.value!)} />
                    <Button
                        icon={<AddIcon />}
                        disabled={!newQuestion}
                        onClick={addNewQuestion} />
                </div>
                <Button
                    style={{ marginTop: "36px" }}
                    fluid
                    primary
                    onClick={saveQuestions}
                    icon={<SaveIcon />}
                    content={pl.createQuestions.confirm} />
            </Segment>
        </div>);

    return (
        <>
            {questionStore.loadQuestionListsRequestStatus.isPending || questionStore.updateQuestionListRequestStatus.isPending ?
            <Loader style={{marginTop: "50vh"}} />
            :
            renderContent()}
        </>
    );
});

export default CreateQuestions;
