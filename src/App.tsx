import React, { useEffect } from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider as ThemeProvider, teamsTheme, teamsDarkV2Theme } from "@fluentui/react-northstar";
import InMeeting from "./inMeeting/components/InMeeting";
import { useMsTeamsInMeetingApps } from "./inMeeting/hooks/hooks";

function App() {
  const inMeetingAppContext = useMsTeamsInMeetingApps();
  let root = document.documentElement;

  useEffect(() => {
    if (inMeetingAppContext.theme === "dark") {
      root.style.setProperty('--background-color-1', "rgb(45, 44, 44)");
      root.style.setProperty('--background-color-2', "rgb(31, 31, 31)");
      root.style.setProperty('--text-color-1', "rgb(255, 255, 255)");
    } else {
      root.style.setProperty('--background-color-1', "rgb(255, 255, 255)");
      root.style.setProperty('--background-color-2', "rgb(243, 242, 241)");
      root.style.setProperty('--text-color-1', "rgb(0, 0, 0)");
    }
  }, [inMeetingAppContext.theme, root.style]);

  return (
    <div className="App">
      <ThemeProvider
        theme={inMeetingAppContext.theme === "dark"
          ? teamsDarkV2Theme
          : teamsTheme}>
        <Router>
          <Switch>
            <Route path="/meeting">
              <InMeeting />
            </Route>
          </Switch>
        </Router>
      </ThemeProvider>
    </div>
  );
}

export default App;
