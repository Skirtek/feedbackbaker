enum msTeamsMeetingAppFrameEnum {
    beforeMeeting = 1,
    duringMeeting = 2,
    afterMeeting = 3,
    settings = 4,
}

export default msTeamsMeetingAppFrameEnum;
