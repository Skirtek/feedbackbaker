export enum msTeamsMeetingRolesEnum {
    organizer = 1,
    presenter = 2,
    none = 3,
}
