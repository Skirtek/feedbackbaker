import { constants } from "../../shared/constants";
import msTeamsMeetingAppFrameEnum from "../enum/msTeamsMeetingAppFrameEnum";

export const getCurrentInMeetingAppStageByFrameContext = (frameContext: string) => {
    switch (frameContext) {
        case constants.frameContext.settings:
            return msTeamsMeetingAppFrameEnum.settings;
        case constants.frameContext.sidePanel:
            return msTeamsMeetingAppFrameEnum.duringMeeting;
        default:
            return msTeamsMeetingAppFrameEnum.beforeMeeting;
    }
};
