import React, { useState } from "react";
import { useMsTeamsInMeetingApps } from "../hooks/hooks";
import msTeamsMeetingAppFrameEnum from "../enum/msTeamsMeetingAppFrameEnum";
import InMeetingAppConfig from "./InMeetingAppConfig";
import CreateQuestions from "../../components/CreateQuestions";
import VotingButtons from "../../components/VotingButtons";
import Reports from "../../components/Reports";

const InMeeting: React.FC = () => {
  const inMeetingAppContext = useMsTeamsInMeetingApps();

  const [showReports, setShowReports] = useState(false);

  const renderContent = () => {
    switch (inMeetingAppContext.meetingStage) {
      case msTeamsMeetingAppFrameEnum.settings:
        return <InMeetingAppConfig />;
      case msTeamsMeetingAppFrameEnum.duringMeeting:
        return <VotingButtons />;
      default:
        return !showReports ? (
        <CreateQuestions onChange={() => setShowReports(true)} />
        ) : (
        <Reports onClose={() => setShowReports(false)}/>
        );
    }
  };

  return renderContent();
};

export default InMeeting;
