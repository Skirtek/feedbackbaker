import React, { useEffect } from "react";
import * as msTeams from "@microsoft/teams-js";
import InMeetingAppConfigIcon from "./icons/InMeetingAppConfigIcon";
import { Header, Text } from "@fluentui/react-northstar";

const InMeetingAppConfig: React.FC = () => {
  useEffect(() => {
    msTeams.settings.setSettings({
      contentUrl: `${window.location.origin}/meeting`,
      entityId: "in-meeting-app",
    });
    msTeams.settings.setValidityState(true);
  }, []);

  return (
    <div className="config-wrapper">
      <InMeetingAppConfigIcon />
      <Header as="h2">Zbieraj feedback</Header>
      <Text size="medium">
        Ułóż pytania, dynamicznie wyświetlaj je podczas spotkania i sprawdzaj
        atrybucję uczestników na bieżąco.
      </Text>
    </div>
  );
};

export default InMeetingAppConfig;
