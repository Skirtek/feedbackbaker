import { useState, useEffect } from "react";
import msTeamsMeetingAppFrameEnum from "../enum/msTeamsMeetingAppFrameEnum";
import * as msTeams from "@microsoft/teams-js";
import { getCurrentInMeetingAppStageByFrameContext } from "../utils/msTeamsInMeetingUtils";

export interface MsTeamsInMeetingAppContext {
    theme: string;
    meetingId: string;
    meetingStage: msTeamsMeetingAppFrameEnum;
}

export const useMsTeamsInMeetingApps = () => {
    const [meetingId, setMeetingId] = useState("");
    const [currentMeetingStage, setCurrentMeetingStage] = useState(msTeamsMeetingAppFrameEnum.beforeMeeting);
    const [currentMsTeamsThemeName, setCurrentMsTeamsThemeName] = useState("default");

    useEffect(() => {
        msTeams.initialize(() => {
            msTeams.getContext((msTeamsContext) => {
                setCurrentMsTeamsThemeName(msTeamsContext.theme!);
                setMeetingId(msTeamsContext.meetingId!);
                setCurrentMeetingStage(getCurrentInMeetingAppStageByFrameContext(msTeamsContext.frameContext!));
                msTeams.registerOnThemeChangeHandler((themeName) => {
                    setCurrentMsTeamsThemeName(themeName);
                });
                msTeams.appInitialization.notifySuccess();
            });
        });
    }, []);

    return {
        theme: currentMsTeamsThemeName,
        meetingId: meetingId,
        meetingStage: currentMeetingStage,
    };
};
